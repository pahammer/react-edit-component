import { useState } from "react";

export default function Home() {
	const [name, setName] = useState("pam");
	const [isEditable, setIsEditable] = useState(false);

	const handleCancel = () => {
		setIsEditable(false);
	};

	const handleSave = (value: string) => {
		setName(value);
		setIsEditable(false);
	};

	const handleNameSelect = () => {
		setIsEditable(true);
	};

	return (
		<div className="h-screen w-screen flex items-center justify-center flex-col space-y-1">
			{isEditable ? (
				<EditableNameComponent
					name={name}
					handleSave={handleSave}
					handleCancel={handleCancel}
				/>
			) : (
				<NameComponent name={name} onSelect={handleNameSelect} />
			)}
		</div>
	);
}

type NameComponentProps = {
	name: string;
	onSelect: () => void;
};

function NameComponent({ name, onSelect }: NameComponentProps) {
	return (
		<div
			onClick={onSelect}
			className="w-36 px-2 h-8 flex items-center justify-center bg-blue-100 overflow-hidden cursor-pointer hover:bg-blue-200"
		>
			{name}
		</div>
	);
}

type EditableNameComponentProps = {
	name: string;
	handleSave: (props: string) => void;
	handleCancel: () => void;
};

function EditableNameComponent({
	name,
	handleSave,
	handleCancel,
}: EditableNameComponentProps) {
	const [tempName, setTempName] = useState(name);
	return (
		<div className="flex flex-row space-x-1">
			<input
				autoFocus
				className="w-36 h-8 bg-blue-100 text-center"
				value={tempName}
				type="text"
				onChange={(e) => setTempName(e.target.value)}
			/>
			<button onClick={() => handleSave(tempName)}>
				{CheckMarkComponent}
			</button>
			<button onClick={handleCancel}>{CancelComponent}</button>
		</div>
	);
}

// HeroIcons
const CheckMarkComponent = (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		fill="none"
		viewBox="0 0 24 24"
		strokeWidth={2}
		stroke="currentColor"
		className="w-6 h-6 bg-green-600 text-white rounded-full p-1"
	>
		<path
			strokeLinecap="round"
			strokeLinejoin="round"
			d="M4.5 12.75l6 6 9-13.5"
		/>
	</svg>
);

const CancelComponent = (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		fill="none"
		viewBox="0 0 24 24"
		strokeWidth={2}
		stroke="currentColor"
		className="w-6 h-6 bg-red-600 text-white rounded-full p-1"
	>
		<path
			strokeLinecap="round"
			strokeLinejoin="round"
			d="M6 18L18 6M6 6l12 12"
		/>
	</svg>
);
