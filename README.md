<br />
<div align="center">
  <h3 align="center">React Code Snippet - OnClick Component to Form</h3>
  <p align="center">
    The code inside pages/index.tsx provides a way to convert a component into its editable form.  
    <br />
    <br />
    Inspired by Trello, developed with React, prettified with tailwind, some typesafety, and of course useState utilization.

![react.js]
![tailwindcss]
![typescript]

</div>

<!-- ABOUT THE PROJECT -->

## Project Demo

_Apologies for the quality of this gif_

![project-gif]

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[project-gif]: images/demo.gif
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[tailwindcss]: https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white
[typescript]: https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white
